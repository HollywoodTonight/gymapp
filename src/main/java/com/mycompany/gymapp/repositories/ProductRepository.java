/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.repositories;

import com.mycompany.gymapp.models.Product;

/**
 *
 * @author andreiiorga
 */
public class ProductRepository extends AbstractHibernateDAO{

    public ProductRepository() {
        setClazz(Product.class);
    }
    
}
