/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.repositories;

import com.mycompany.gymapp.models.Admin;

/**
 *
 * @author andreiiorga
 */
public class AdminRepository extends AbstractHibernateDAO {
    public AdminRepository(){
        setClazz(Admin.class);
    }
}
