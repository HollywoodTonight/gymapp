/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.models;

import com.mycompany.gymapp.entities.ProductType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alexandru.tudoran
 */
@Entity
@Table
public class Product implements Serializable{

    
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}

	private String productName;
	private ProductType type;
	private double productPrice;
        private int count = 1;

	public Product(String pProductName, ProductType pType, double d, int pCount) {
		this.productName = pProductName;
		this.type = pType;
		this.productPrice = d;
                this.count = pCount;
	}
        
        public Product(String pProductName, ProductType pType, double d) {
		this.productName = pProductName;
		this.type = pType;
		this.productPrice = d;
	}

	public Product(ProductType ptype) {
		this.type = ptype;
	}

	public Product() {

	}

        public int getCount() {
             return count;
        }
        
        public void setcount(int pCount){
            this.count = pCount;
        }

	@Override
	public String toString() {
		return "Product " + productName + " of type " + type + " has a price of " + productPrice;
	}

	public boolean Validate() {
		boolean isValid = true;

		if (this.productName == null)
			isValid = false;
		if (this.productPrice == 0)
			isValid = false;

		return isValid;
	}
}
