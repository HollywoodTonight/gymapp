/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.models;

import com.mycompany.gymapp.models.Customer;
import com.mycompany.gymapp.models.Product;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alexandru.tudoran
 */
@Entity
@Table
public class Transaction implements Serializable{

        @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
        
    private Customer customer;
    private List<Product> products;
    private Date dateOfTransaction;
    private int transactionID;

    public Transaction(Customer c, List<Product> productList, Date date, int pTransactionID) {
        this.customer = c;
        this.products = productList;
        this.dateOfTransaction = date;
        this.transactionID = pTransactionID;
    }

    public Transaction(int ID) {
        this.transactionID = ID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Product> getProduct() {
        return products;
    }

    public void setProduct(List<Product> productList) {
        this.products = productList;
    }

    public Transaction() {

    }

    public int getTransactionID() {
        return transactionID;
    }

    public Transaction getTransaction() {

        if (customer != null && products != null) {
            return this;
        } else {
            return null;
        }

    }

    public Date getDateOfTransaction() {
        return dateOfTransaction;
    }

    public void setDateOfTransaction(Date dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    @Override

    public String toString() {
        return ("Transaction with the ID " + this.transactionID
                + " was made by " + customer.getFirstName() + " " + customer.getLastName()
                + " on " + this.dateOfTransaction
                + " with the order of " + GenerateTransactionProducts()
                + "with a price of " + GenerateTransactionPrice());
    }

    public String GenerateTransactionProducts(){
        String allProducts = "";
        
        if (products.isEmpty()){
            allProducts = " The product list is empty";
        }
        else if (products.size() > 1) {
            for (Product p : products) {
                allProducts += p.getProductName() + ", ";
            }
        }
        else{
            for (Product p : products) {
                allProducts += p.getProductName();
            }
        }
        return allProducts;
    }

    public double GenerateTransactionPrice() {
        double totalPrice = 0;
        for (Product p : products) {
            totalPrice += p.getProductPrice();
        }
        return totalPrice;
    }
}
