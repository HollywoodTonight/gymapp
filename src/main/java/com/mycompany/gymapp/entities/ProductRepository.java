/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.entities;

import com.mycompany.gymapp.models.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alexandru.tudoran
 */
public class ProductRepository {

	/*
	 * Retrieve one Customer
	 * 
	 * @Params - customerID
	 */
	public Product Retrieve(ProductType type) {

		// Temporary Code to test the retrieve method
		Product product = new Product(ProductType.Proteine);

		if (product.getType() == ProductType.Proteine) {
			product.setProductName("ON Protein");
			product.setProductPrice(40);
		}
		// Code that retrieves the defined customer
		return product;
	}

	/*
	 * Retrieve all the customers
	 */
	public List<Product> Retrieve() {

		// Code that retrieves all customers
		List<Product> productList = new ArrayList<Product>();
		return productList;
	}
}
