/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.entities;

/**
 *
 * @author alexandru.tudoran
 */
public enum ProductType {
	Baton_Proteic,
	Baton_Slabit,
	Proteine,
	Apa_1L,
	Apa_05L
}
