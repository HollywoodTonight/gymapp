/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.entities;

import com.mycompany.gymapp.models.Customer;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author alexandru.tudoran
 */
public final class MyCellRenderer
        extends JLabel
        implements ListCellRenderer<Customer> {

    @Override
    public Component getListCellRendererComponent(
            final JList<? extends Customer> list,
            final Customer value, final int index,
            final boolean isSelected, final boolean cellHasFocus) {

        final String shownValue = value.getFirstName() + " "
                + value.getLastName();

        setText(shownValue);

        System.out.println("this object is " + this.toString());
        System.out.println("cell renderer for " + shownValue);

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setForeground(list.getForeground());
            if (value.IsActive()) {
                System.out.println("showing green background for " + shownValue);
                setBackground(Color.green);
            } else {
                System.out.println("showing list background for " + shownValue);
                setBackground(list.getBackground());
            }
        }
        setOpaque(true);

        return this;
    }
}
